import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please select star type[1-4,5 is Exit]: ");
            int num1 = sc.nextInt();
            if (num1 == 1) {
                System.out.print("Please input number: ");
                int num2 = sc.nextInt();
                for(int i = 0;i<num2;i++){
                    for(int j = 0;j<=i;j++){
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }else if(num1 == 2){
                System.out.print("Please input number: ");
                int num2 = sc.nextInt();
                for (int i = num2; i >0; i--) {
                    for (int j = 0; j < i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }

            }else if(num1 == 3){
                System.out.print("Please input number: ");
                int num2 = sc.nextInt();
                for (int i = 0; i <num2; i++) {
                    for (int j = 0; j < num2; j++) {
                        if(j>=i){
                            System.out.print("*");
                        }else{
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            }else if(num1 == 4){
                System.out.print("Please input number: ");
                int num2 = sc.nextInt();
                for (int i = num2; i > 0; i--) {
                    for (int j = 0; j <= num2; j++) {
                        if(j>=i){
                            System.out.print("*");
                        }else{
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            }else if(num1 == 5){
                System.out.println("Bye bye!!!");
                break;
            }else{
                System.out.println("Eror:Please input number between 1-5");
            }

        }
        sc.close();   
        
    }
}

